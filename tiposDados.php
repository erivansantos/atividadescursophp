<?

/**
 * Criado por: Erivan Santos
 * Curso: PHP 7 Online
 * Plataforma: Udemy
 * PHP 7.1
 */

///////////////// TIPOS DE DADOS SIMPLES /////////////////

 //String
$nome = "Erivan";
$site = 'www.google.com.br';

//Inteiro
$ano = 2018;

//Float
$salario = 5500.99;


//Booleano
$dolar = false;

///////////////// TIPOS DE DADOS COMPOSTOS /////////////////

//Array
$frutas = array("abacaxi","morango","manga");

//Printando a posição
echo $frutas[2];

//Objeto
$nascimento = new DateTime();

var_dump($nascimento);


///////////////// TIPOS DE DADOS ESPECIAIS /////////////////

//Arquivo
$arquivo = fopen("variaveis.php", "r");

var_dump($arquivo);



